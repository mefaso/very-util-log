package com.joyrec.util.log.future

import com.joyrec.util.log.Logable

trait WithDebugUtils extends Logable {
  def debugVal[R](key: =>S)(fn: => R) = {
    val r = fn
    debug(s"[$key]=[${r}]")
    r
  }

}