package com.joyrec.util.log.future

import ws.very.util.lang.StopWatch
import com.joyrec.util.log.Logable

trait StopwatchLog extends Logable {

  def debugStopWatch[R](token: => String)(fn: => R) = StopWatch({ fn }) { t =>
    log.debug(token + " takes " + t)
  }
}