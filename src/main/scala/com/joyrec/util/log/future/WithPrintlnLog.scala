package com.joyrec.util.log.future

import com.joyrec.util.log.Logable

trait WithPrintlnLog extends Logable {
  def println(any: Any) = debug(mes = any)
}