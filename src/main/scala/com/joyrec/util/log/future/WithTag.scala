package com.joyrec.util.log.future

trait WithTag {
  def tag: String
}

trait WithClass {
  lazy val tag = this.getClass().getName()
}