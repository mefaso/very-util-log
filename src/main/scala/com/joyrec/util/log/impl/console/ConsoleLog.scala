package com.joyrec.util.log.impl.console

import com.joyrec.util.log.Log
import com.joyrec.util.log.Logable
import com.joyrec.util.log.future.WithClass
import com.joyrec.util.log.future.AllFuture
import com.joyrec.util.log.impl.slf4j.ClassSlf4j

trait ConsoleLog extends Log {
  def errorEnabled = true
  def warnEnabled = true
  def debugEnabled = true
  def infoEnabled = true
  private def printLog(`type`: String, mes: Any, e: Throwable) = {
    println(`type` + ":" + mes)
    if (e != null) e.printStackTrace()
  }
  def error(mes: => Any = "", e: Throwable = null) = printLog("error", mes, e)
  def info(mes: => Any = "", e: Throwable = null) = printLog("info", mes, e)
  def debug(mes: => Any = "", e: Throwable = null) = printLog("debug", mes, e)
  def warn(mes: => Any = "", e: Throwable = null) = printLog("warn", mes, e)
}
object ConsoleLog extends ConsoleLog

trait WithConsoleLog extends Logable {
  protected def log = ConsoleLog
}



trait ClassConsoleLog extends WithClass with WithConsoleLog
trait ClassConsoleDebugLog extends ClassConsoleLog with AllFuture

