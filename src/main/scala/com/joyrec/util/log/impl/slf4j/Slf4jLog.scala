package com.joyrec.util.log.impl.slf4j

import org.slf4j.LoggerFactory
import com.joyrec.util.log.Log
import com.joyrec.util.log.future.WithTag
import com.joyrec.util.log.future.WithClass
import com.joyrec.util.log.Logable
import com.joyrec.util.log.future.WithPrintlnLog
import com.joyrec.util.log.future.StopwatchLog
import com.joyrec.util.log.future.AllFuture

trait Slf4jLog extends Log {
  def clazzName: String
  private val log: org.slf4j.Logger = LoggerFactory.getLogger(clazzName)
  def errorEnabled = log.isDebugEnabled()
  def warnEnabled = log.isWarnEnabled()
  def debugEnabled = log.isDebugEnabled()
  def infoEnabled = log.isInfoEnabled()
  def error(mes: => Any = "", e: Throwable = null) =
    log.error(mes.toString, e)
  def info(mes: => Any = "", e: Throwable = null) =
    log.info(mes.toString, e)
  def debug(mes: => Any = "", e: Throwable = null) =
    log.debug(mes.toString, e)
  def warn(mes: => Any = "", e: Throwable = null) =
    log.warn(mes.toString, e)
}

trait WithSlf4j extends Logable with WithTag {
  lazy val log: Log = new Slf4jLog {
    def clazzName: String = tag
  }
}
trait ClassSlf4j extends WithClass with WithSlf4j
trait ClassSlf4jDebugLog extends ClassSlf4j with AllFuture

