name := "very-util-log"

version := "0.0.0"

scalaVersion := "2.11.4"

resolvers +=
  "oschina maven" at "http://maven.oschina.net/content/groups/public/"

resolvers +=
  "oschina maven thirdparty" at "http://maven.oschina.net/content/repositories/thirdparty/"

libraryDependencies += "ch.qos.logback" % "logback-core" % "1.1.0"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.0"

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.7"



lazy val lang = RootProject(file("../very-util-lang"))

lazy val log = project.in(file(".")).dependsOn(lang)